habitual = ("patatas", "leche", "pan")


def obtener_compra_especifica():
    compra_especifica = []
    while True:
        item = input("¿Qué deseas comprar en esta ocasión? (Pulsa Enter para terminar): ")
        if item == "":
            break
        compra_especifica.append(item)
    return compra_especifica


def generar_lista_compra():
    compra_especifica = obtener_compra_especifica()

    compra_total = list(habitual) + compra_especifica

    compra_total = list(set(compra_total))

    print("Lista de compra:")
    for item in compra_total:
        print(item)

    num_elementos_habituales = len(habitual)
    num_elementos_especificos = len(compra_especifica)
    num_elementos_totales = len(compra_total)

    print(f"Elementos habituales: {num_elementos_habituales}")
    print(f"Elementos específicos: {num_elementos_especificos}")
    print(f"Elementos totales: {num_elementos_totales}")


def main():
    generar_lista_compra()


if __name__ == "__main__":
    main()
